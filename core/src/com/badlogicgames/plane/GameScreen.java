package com.badlogicgames.plane;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by damviod on 22/04/16.
 */
public class GameScreen extends Game {
SpriteBatch batch;
    BitmapFont font;



    @Override
    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        this.setScreen(new MainMenu(this));
    }
    public  void render(){super.render();}

    public void dispose()
    {
        batch.dispose();
        font.dispose();

    }


}
